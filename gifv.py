#!/usr/env python3
import argparse
import subprocess
import sys
import io

def parse_time(time):
    try:
        return float(time)
    except ValueError:
        # TODO: support partial times (ie 1:30.20)
        parts = time.split(':')
        parts.reverse()
        time = 0
        for i, p in enumerate(parts):
            time += int(p) * max(1, 60 * i)
        return time


picture_sub_codecs = ['dvd_subtitle']


def is_picture_subtitles(input_file, subtitle_index):
    proc = subprocess.run(["ffprobe", "-select_streams", "s:{}".format(subtitle_index), "-print_format", "flat", "-show_streams", input_file], stdout=subprocess.PIPE, universal_newlines=True)
    matches = [l for l in proc.stdout.split('\n') if l.startswith('streams.stream.0.codec_name')]
    if len(matches) != 1:
        raise RuntimeError("No subtitle stream found")
    else:
        return matches[0].split("=")[1].replace('"', '') in picture_sub_codecs


def gifv(filepath, output_file, start=None, end=None, size=None, subtitles=False, subtitle_index=0):
    start_pts = parse_time(start)
    duration = parse_time(end) - start_pts
    video_filters = []
    complex_filters = []
    args = ['ffmpeg', '-ss', str(start_pts), '-i', filepath, '-t', str(duration), '-an', '-preset', str(7), '-c:v', 'libx264', '-y']
    if subtitles:
        if is_picture_subtitles(filepath, subtitle_index):
            complex_filters += ['[0:v][0:s:1]overlay[v]']
        else:
            video_filters += ['setpts=PTS+{}/TB'.format(start_pts), 'subtitles=\'{}\':si={}'.format(filepath, subtitle_index), 'setpts=PTS-STARTPTS']
    if video_filters and complex_filters:
        raise RuntimeError("cannot have video filters and complex filters")
    if video_filters:
        args += ['-vf', ','.join(video_filters)]
    if complex_filters:
        args += ['-filter_complex', ','.join(complex_filters), '-map', '[v]']
    args += [output_file]
    with subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT) as proc:
        out = io.TextIOWrapper(proc.stdout, newline='\n')
        while proc.poll() is None:
            sys.stdout.write(out.read(100))
        sys.stdout.write(out.read())


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--subtitles', action='store_true', dest='subtitles')
    parser.add_argument('--subtitle-index', dest='subtitle_index', type=int, default=0)
    parser.add_argument('--start', '-s', type=str)
    parser.add_argument('--end', '-e', type=str)
    parser.add_argument('--size', type=int)
    parser.add_argument('file')
    parser.add_argument('output_file', nargs='?', default='output.mp4')
    args = parser.parse_args()
    gifv(args.file, args.output_file, args.start, args.end, args.size, args.subtitles, args.subtitle_index)

    
if __name__ == '__main__':
    main()
